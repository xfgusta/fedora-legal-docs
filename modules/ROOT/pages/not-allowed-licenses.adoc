= *Not-Allowed* Licenses

This page lists the licenses that are *not-allowed* for use in Fedora. This information is generated from the data maintained in the https://gitlab.com/fedora/legal/fedora-license-data[Fedora License Data] repository.

[options=header,valign=middle,stripes=even,format=tsv,separator=|]
|===
include::partial$not-allowed.dsv[]
|===
