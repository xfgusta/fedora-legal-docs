= *Allowed* Licenses

This page lists the licenses that are *allowed* for use in Fedora. If a license is marked as *allowed*, it is allowed for any use. If a license is marked as *allowed-fonts*, *allowed-content*, *allowed-documentation*, or *allowed-firmware*, then it is allowed only for certain categories of material as described in xref:license-approval.adoc[License Approval]. This information is generated from data maintained in the https://gitlab.com/fedora/legal/fedora-license-data[Fedora License Data] repository.

[options=header,valign=middle,stripes=even,format=tsv,separator=|]
|===
include::partial$allowed.dsv[]
|===
